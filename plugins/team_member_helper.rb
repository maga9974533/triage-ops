# frozen_string_literal: true

require_relative '../lib/team_member_helper'

Gitlab::Triage::Resource::Context.include TeamMemberHelper
