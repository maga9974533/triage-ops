# frozen_string_literal: true

require_relative 'base'

module Triage
  module PipelineFailure
    module Config
      class MasterBranch < Base
        INCIDENT_PROJECT_ID = '40549124' # gitlab-org/quality/engineering-productivity/master-broken-incidents
        INCIDENT_LABELS = ['Engineering Productivity'].freeze
        INCIDENT_TEMPLATE = <<~MARKDOWN.freeze
          #{DEFAULT_INCIDENT_SUMMARY_TABLE}

          ### General guidelines

          Follow the [Broken `master` handbook guide](https://about.gitlab.com/handbook/engineering/workflow/#broken-master).

          %<attribution_body>s

          ### Investigation

          **Please capture your investigation steps in the `Investigation Steps` thread.**

          1. If the failure is new, and looks like a potential flaky failure, you can retry the failing jobs with `@gitlab-bot retry_pipeline %<pipeline_id>s`.
          1. If the failure looks like a broken `master`, communicate the broken `master` in Slack using the "Broadcast Master Broken" workflow:
            - Click the Shortcut lightning bolt icon in the `#master-broken` channel and select "Broadcast Master Broken".
            - Click "Continue the broadcast" after the automated message in `#master-broken`.

          ### Root Cause Analysis

          1. It is important to categorize the incident by its root cause to identify corrective actions and to track data for furture references.
            - Root cause labels can be found in the [Broken `master` handbook guide](https://about.gitlab.com/handbook/engineering/workflow/#triage-dri-responsibilities). Search for `Please set the appropriate ~master-broken:* label from the list below`.

          ### Pre-resolution

          If you believe that there's an easy resolution by either:

          - Reverting a particular merge request.
          - Making a quick fix (for example, one line or a few similar simple changes in a few lines).
            You can create a merge request, assign to any available maintainer, and ping people that were involved/related to the introduction of the failure.
            Additionally, a message can be posted in `#backend_maintainers` or `#frontend_maintainers` to get a maintainer take a look at the fix ASAP.

          In both cases, make sure to add the ~"pipeline:expedite" label, and `master:broken` or `master:foss-broken` label, to speed up the `master`-fixing pipelines.

          ### Resolution

          Follow [the Resolution steps from the handbook](https://about.gitlab.com/handbook/engineering/workflow/#responsibilities-of-the-resolution-dri).
        MARKDOWN

        SLACK_CHANNEL = 'master-broken'

        def self.match?(event)
          event.on_instance?(:com) &&
            event.project_path_with_namespace == 'gitlab-org/gitlab' &&
            !event.merge_request_pipeline? &&
            event.ref == 'master' &&
            event.source_job_id.nil?
        end

        def incident_project_id
          INCIDENT_PROJECT_ID
        end

        def incident_template
          INCIDENT_TEMPLATE
        end

        def incident_labels
          master_broken_label =
            if event.project_path_with_namespace.end_with?('gitlab-foss')
              'master:foss-broken'
            else
              'master:broken'
            end

          [*INCIDENT_LABELS, master_broken_label]
        end

        def default_slack_channels
          [SLACK_CHANNEL]
        end

        def auto_triage?
          true
        end
      end
    end
  end
end
