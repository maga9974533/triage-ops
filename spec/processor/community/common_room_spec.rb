# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/common_room'
require_relative '../../../triage/job/common_room_job'

RSpec.describe Triage::CommonRoom do
  include_context 'with event', Triage::MergeRequestNoteEvent do
    let(:event_attrs) do
      {
        from_gitlab_org?: from_gitlab_org,
        from_gitlab_com?: from_gitlab_com,
        wider_community_author?: wider_community_author,
        confidential?: confidential
      }
    end
  end

  let(:wider_community_author) { true }
  let(:from_gitlab_com) { false }
  let(:from_gitlab_org) { true }
  let(:confidential) { false }

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['issue.note', 'merge_request.note']

  describe '#applicable?' do
    include_examples 'event is applicable'

    context 'when not wider_community_author?' do
      let(:wider_community_author) { false }

      include_examples 'event is not applicable'
    end

    context 'when from_gitlab_com?' do
      let(:from_gitlab_org) { false }
      let(:from_gitlab_com) { true }

      include_examples 'event is applicable'
    end

    context 'when not from_gitlab_org? and not from_gitlab_com?' do
      let(:from_gitlab_org) { false }

      include_examples 'event is not applicable'
    end

    context 'when confidential' do
      let(:confidential) { true }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'enqueus CommonRoomJob with the event' do
      expect(Triage::CommonRoomJob).to receive(:perform_async).with(
        event
      )

      subject.process
    end
  end
end
